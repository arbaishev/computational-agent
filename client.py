import tornado.httpclient
import tornado.ioloop
import tornado.web
import mimetypes
import os.path
import configparser
import argparse
import json
import socket


class Client:
    def __init__(self, configuration, url, http_client):
        self.configuration = configuration
        self.server_url = 'http://' + url + ':' + str(configuration['port'])
        self.http_client = http_client
        if configuration['id'] != None:
            self.cookie = 'ID=' + configuration['id'] + '; Path=/'
        else:
            file_path = os.path.join(os.getcwd(), 'client_id.txt')
            if os.path.exists(file_path):
                f = open(file_path)
                self.cookie = f.read()


    def GetId(self):
        request = tornado.httpclient.HTTPRequest(self.server_url, method='GET',
                                headers={'Accept': 'application/json'})
        response = self.http_client.fetch(request)
        self.cookie = response.headers['Set-Cookie']
        file_wth_id = open(os.path.join(os.getcwd(), 'client_id.txt'), 'w')
        file_wth_id.write(self.cookie)
        file_wth_id.close()
        return self.cookie

    def ContentType(self, filename):
        guessed = mimetypes.guess_type(filename)
        if guessed[0] != None:
            return guessed[0]
        else:
            return 'application/octet-stream'

    def GetMultipartBody(self, boundary, files, command):
        lines = []
        endln = '\r\n'
        lines += [('--' + boundary).encode('utf-8')] + [('Content-Disposition: form-data; name="command"').encode('utf-8')] + [b''] + [(command).encode('utf-8')]
        for i in range(len(files)):
            lines += [('--' + boundary).encode('utf-8')] + [('Content-Disposition: form-data; name="file%d"; filename=%s'\
                     % (i + 1, os.path.basename(files[i]))).encode('utf-8')] +\
                    [('Content-Type: ' + self.ContentType(files[i])).encode('utf-8')] + [b''] + [b'']
            try:
                with open(files[i], 'rb') as f:
                    lines[len(lines) - 1] = f.read()
            except FileNotFoundError:
                raise FileNotFoundError('File: ' + files[i] + ' not found')
        lines += [('--' + boundary + '--').encode('utf-8')] + [b'']
        return (endln).encode('utf-8').join(lines)

    def SubmitTask(self, command, files):
        url = self.server_url + '/tasks'
        boundary = '---!Boundary'
        body = self.GetMultipartBody(boundary, files, command)
        headers = {'Content-Type': 'multipart/form-data; boundary=' + boundary,
                'Content-Length': str(len(body)), 'Accept': 'application/json',
                'File-Num': str(len(files)), 'Cookie': self.cookie}
        request = tornado.httpclient.HTTPRequest(url, method='POST', headers=headers, body=body)
        try:
            response = self.http_client.fetch(request)
            response = json.loads(response.body.decode('utf-8'))
        except ConnectionError:
            raise ConnectionError("Failed: can not connect to server")
        except (TypeError, ValueError):
            raise SubmitError('Client error: bad response')
        return response['task_id']


    def GetTasks(self):
        url = self.server_url + '/tasks'
        request = tornado.httpclient.HTTPRequest(url, method='GET',
                    headers={'Accept': 'application/json', 'Cookie': self.cookie})
        try:
            response = self.http_client.fetch(request)
            response = json.loads(response.body.decode('utf-8'))
        except ConnectionError:
            raise ConnectionError("Failed: can not connect to server")
        except (TypeError, ValueError):
            raise GetTasksError('Failed to get tasks list: bad response')
        return response

    def CompletedTasks(self):
        tasks = self.GetTasks()
        completed_tasks = []
        for key in tasks:
            if tasks[key]['state'] == 'Done' or tasks[key]['state'] == 'Failed':
                completed_tasks.append(key)
        return completed_tasks

    def GetFiles(self, task):
        flag = False
        url = self.server_url + '/tasks/' + task
        if os.access(self.configuration['outdir'], os.W_OK):
            if not os.path.exists(self.configuration['outdir']):
                os.makedirs(self.configuration['outdir'])
            if not os.path.exists(os.path.join(self.configuration['outdir'], task)):
                os.makedirs(os.path.join(self.configuration['outdir'], task))
        else:
            raise GetFilesError("Download files failed: does not have permission to write to output directory")

        request = tornado.httpclient.HTTPRequest(url, method='GET',
                headers={'Accept': 'application/json', 'Cookie': self.cookie})
        try:
            response = self.http_client.fetch(request)
            response = json.loads(response.body.decode('utf-8'))
        except ConnectionError:
            raise ConnectionError("Failed: can not connect to server")
        except (TypeError, ValueError):
            raise GetFilesError('Download files failed : bad response')
        message = ''
        try:
            files = response['files']
            for file in files:
                flag = False
                url = self.server_url + '/tasks/' + task + '/' + file
                request = tornado.httpclient.HTTPRequest(url, method='GET',
                                headers={'Accept': 'application/json', 'Cookie': self.cookie})
                response = self.http_client.fetch(request)
                with open(os.path.join(self.configuration['outdir'], task, file), 'wb') as f:
                    f.write(response.body)
                flag = True
        except Exception as e:
            message = 'file not found or can not be read'
            flag = False

        return flag, message

        
class SubmitError(Exception):
    def __init__(self, message):
        self.message = message


class GetFilesError(Exception):
    def __init__(self, message):
        self.message = message


class GetTasksError(Exception):
    def __init__(self, message):
        self.message = message


def get_client_config():
    configuration = {}
    parser = argparse.ArgumentParser(description='Client')
    parser.add_argument('--id', help='client id', default=None, metavar='ID')
    parser.add_argument('--action', help='action(post, tasks, download, get-id)', default='get-id', metavar='act',
                        choices = ('post', 'tasks', 'download', 'get-id'))
    parser.add_argument('--outdir', help='directory for downloaded files', default='results')
    parser.add_argument('--indir', help='directory with files to post', default='')
    args = parser.parse_args()
    configuration = {'action':args.action, 
                'task': {}, 'outdir':args.outdir, 
                'indir':args.indir, 'id':args.id}
    config = configparser.ConfigParser()
    config.read('client_config.ini')
    try:
        if config['general']['port']:
            configuration['port'] = int(config['general']['port'])
        if config['general']['url']:
            configuration['url'] = config['general']['url']
    except:
        print('Config file is corrupted. Set default values')
    configuration['url'] = 'localhost'
    configuration['port'] = 8888
    return configuration


if __name__ == "__main__":
    configuration = get_client_config()
    http_client = tornado.httpclient.HTTPClient()
    client = Client(configuration, configuration['url'], http_client)
    try:

        if configuration['action'] == 'get-id':
            print(client.GetId())

        if configuration['action'] == 'post':
            command = input('command:')
            files = []
            if configuration['indir'] == '':
                inp_file = input('filename:')
                while inp_file != '':
                    files.append(inp_file)
                    inp_file = input('filename:')
            else:
                for d, dirs, f in os.walk(configuration['indir']):
                    for elem in f:
                        files.append(os.path.join(d,elem))
            if files != []:
                new_task = client.SubmitTask(command, files)
            else:
                raise SubmitError('No files selected') 

            print('\nCreated task: %s\n' % new_task)

        if configuration['action'] == 'tasks':
            tasks = client.GetTasks()
            output = ''
            for key in tasks.keys():
                output += '\n'
                for task_key in sorted(tasks[key]):
                    output += task_key + ': ' + str(tasks[key][task_key]) + '\n'
            print(output)
        
        if configuration['action'] == 'download':
            comp_tasks = client.CompletedTasks()
            print('\nCompleted tasks are available for download:\n')
            for elem in comp_tasks:
                print(elem)
            task = input('\nEnter task ID or "all" for download all tasks:\n')
            req_tasks = []
            if task == 'all':
                for key in comp_tasks:
                    req_tasks.append(key)
            else:
                req_tasks.append(task)
            for t in req_tasks:
                flag, message = client.GetFiles(t)
                if flag:
                    print('Download task %s completed' % t)
                else:
                    print('Download task %s failed: %s' % (t, message))

    except Exception as error:
        print("\n%s" % error)
    except KeyboardInterrupt:
        print("\nClosing client...")
    finally:
        http_client.close()




