import argparse
import os
import os.path
import signal
import subprocess
import sys
import uuid
import time
import threading
import queue
import configparser
import tornado.httpserver
import tornado.ioloop
import tornado.web
import stat
import json


'''command:
vina --receptor protein.pdbqt --ligand ligand.pdbqt --config config.txt --exhaustiveness 1 --cpu 2
'''


class Application(tornado.web.Application):
    def __init__(self, configuration):
        self.configuration = configuration
        if not os.path.exists("tasks"):
            os.makedirs("tasks")
        self.tasks = {}
        self.task_queue = queue.Queue()
        self.clients = {}
        handlers = [
            (r"/", IndexHandler, dict(clients_ids=self.clients)),
            (r"/tasks", SubmitHandler, dict(tasks=self.tasks, task_queue = self.task_queue, configuration=self.configuration, clients_ids=self.clients)),
            (r"/tasks/([^/]+)", TaskHandler, dict(tasks=self.tasks, clients_ids=self.clients)),
            (r"/tasks/([^/]+)/([^/]+)", FileHandler, dict(tasks=self.tasks, clients_ids=self.clients))
        ]

        tornado.web.Application.__init__(self, handlers)


class IndexHandler(tornado.web.RequestHandler):
    def initialize(self, clients_ids):
        self.clients_ids = clients_ids

    def get(self):
        if not self.get_cookie('ID') in self.clients_ids.keys():
            client_id = str(uuid.uuid4())
            while client_id in self.clients_ids.keys():
                client_id = str(uuid.uuid4())
            self.set_cookie('ID', client_id)
            self.clients_ids[client_id] = []
            print('New client:' + client_id + '\n' )
        self.render("index.html")


class SubmitHandler(tornado.web.RequestHandler):
    def initialize(self, tasks, task_queue, configuration, clients_ids):
        self.tasks = tasks
        self.task_queue = task_queue
        self.configuration = configuration
        self.clients_ids = clients_ids

    def get(self):
        if not self.get_cookie('ID') in self.clients_ids.keys():
            raise tornado.web.HTTPError(403)
        if self.request.headers['Accept'] == 'application/json':
            ids = {}
            for key in self.tasks.keys():
                if self.get_cookie('ID') != self.tasks[key]['client']:
                    continue
                ids[key] = {}
                ids[key]['ID'] = key
                ids[key]['state'] = self.tasks[key]['state']
                if (self.tasks[key]['state'] == 'Failed' or
                        self.tasks[key]['state'] == 'Done' and
                        self.tasks[key]['return_code'] != None):
                    ids[key]['return_code'] = self.tasks[key]['return_code']
            self.write(json.dumps(ids))
            self.set_header("Content-Type", "application/json")
        else:
            self.write('<b>Tasks:</b><br>')
            for key in self.tasks.keys():
                if self.get_cookie('ID') != self.tasks[key]['client']:
                    continue
                self.write('<p><a href="/tasks/%s">%s</a> is %s ' %\
                           (key, key, (self.tasks[key]['state']).lower()))
                if ((self.tasks[key]['state'] == 'Failed' or
                        self.tasks[key]['state'] == 'Done') and
                        self.tasks[key]['return_code'] != None):
                    self.write('with return code %d' % self.tasks[key]['return_code'])
            self.write('<p><a href="/">New task</a><br>')
        self.finish()

    def post(self):
        if self.get_cookie('ID') not  in self.clients_ids.keys():
            raise tornado.web.HTTPError(403)
        task_id = str(uuid.uuid4())
        while task_id in self.tasks.keys():
            task_id = str(uuid.uuid4())
        task_dir = os.path.join("tasks", task_id)
        os.makedirs(task_dir)
        command = self.get_argument('command')
        if self.request.headers['Accept'] == 'application/json':
            file_num = int(self.request.headers['File-Num'])
        else:
            file_num = self.configuration['numbers_of_files']
        for i in range(1, file_num + 1):
            file_id = 'file' + str(i)
            if file_id in self.request.files:
                file = self.request.files[file_id][0]
                filename = file['filename']
                with open(os.path.join(task_dir, filename), 'wb') as output_file:
                    output_file.write(file['body'])

        self.tasks[task_id] = {
                               'command': command, 'client': self.get_cookie('ID'),
                               'return_code': None, 'files': [], 'state': 'Waiting', 'Error': ''
                               }

        self.task_queue.put(task_id)
        self.clients_ids[self.get_cookie('ID')].append(task_id)
        task_address = "/tasks/" + task_id
        self.set_status(201)
        self.set_header('Location', task_address)
        if self.request.headers['Accept'] == 'application/json':
            self.write(json.dumps(dict([('task_id', task_id)])))
        else:
            self.write("Created task: <a href=\"%s\">%s</a><br>" % (task_address, task_id))
            print('[%s] Created task: %s' % (self.get_cookie('ID'), task_id))        
            self.write('<p><a href="/tasks">All tasks</a><br>')
        self.finish()


class TaskHandler(tornado.web.RequestHandler):
    def initialize(self, tasks, clients_ids):
        self.tasks = tasks
        self.clients_ids = clients_ids

    def get(self, task_id):
        if self.get_cookie('ID') not in self.clients_ids.keys():
            raise tornado.web.HTTPError(403)
        if self.get_cookie('ID') != self.tasks[task_id]['client']:
            raise tornado.web.HTTPError(403)
        if task_id in self.tasks:
            content_type = self.request.headers['Accept']
            response = {}
            response['state'] = self.tasks[task_id]['state']
            if content_type == "application/json":
                if self.tasks[task_id]['state'] == 'Failed' or self.tasks[task_id]['state'] == 'Done':
                    response['files'] = self.tasks[task_id]['files']
                    self.write(json.dumps(response))
                    self.set_header("Content-Type", "application/json")
                    self.finish()
            else:
                if self.tasks[task_id]['state'] == 'Failed':
                    self.write(self.tasks[task_id]['state'] + "<br/>")
                    self.write("Error: " + self.tasks[task_id]['Error'] + "<br/>")
                    self.write("Exit code:" + str(self.tasks[task_id]['return_code']) + "<br/>" + "Results:<br/>")
                    for filename in self.tasks[task_id]['files']:
                        self.write('<br><a href="%s">%s</a>' %
                                   ('/tasks/' + task_id + '/' + filename, filename))
                
                elif self.tasks[task_id]['state'] == 'Done':
                    self.write(self.tasks[task_id]['state'] + "<br/>")
                    self.write("Exit code:" + str(self.tasks[task_id]['return_code']) + "<br/>" + "Results:<br/>")
                    for filename in self.tasks[task_id]['files']:
                        self.write('<br><a href="%s">%s</a>' %
                                   ('/tasks/' + task_id + '/' + filename, filename))

                else:
                    self.write("Task " + task_id + " is " + (self.tasks[task_id]['state']).lower() + "<br/>")
        else:
            raise tornado.web.HTTPError(404)


class FileHandler(tornado.web.RequestHandler):
    def initialize(self, tasks, clients_ids):
        self.tasks = tasks
        self.clients_ids = clients_ids

    def get(self, task_id, file_name):
        if not self.get_cookie('ID') in self.clients_ids.keys():
            raise tornado.web.HTTPError(403)
        if not (task_id in self.clients_ids[self.get_cookie('ID')]):
            raise tornado.web.HTTPError(403)
        print(("[%s] Accessed file:" % self.get_cookie('ID')) +\
              task_id + "/" + file_name)
        if os.path.exists(os.path.join("tasks", task_id)):
            file_path = os.path.join("tasks", task_id, file_name)
            if not os.path.exists(file_path):
                raise tornado.web.HTTPError(404)
            self.set_header('Content-Type', 'application/force-download')
            self.set_header('Content-Disposition', 'attachment; filename=%s' % file_name)
            with open(file_path, "rb") as f:
                while True:
                    data = f.read(4096)
                    if not data:
                        break
                    self.write(data)
            self.finish()
        else:
            raise tornado.web.HTTPError(404)


class Worker(threading.Thread):

    def __init__(self, tasks, task_queue, time_limit):
        threading.Thread.__init__(self)
        self.tasks = tasks
        self.task_queue = task_queue
        self.time_limit = time_limit
        self.stopping = False

    def run(self):
        while not self.stopping:
            try:
                task = self.task_queue.get(block=False)
                self.tasks[task]['state'] = 'Running'
                Process(self.tasks[task], task, self.time_limit)
                self.task_queue.task_done()
            except queue.Empty:
                time.sleep(0.05)

    def stop(self):
        self.stopping = True


def get_agent_config():
    config = configparser.ConfigParser()
    configuration = {}
    try:
        config.read('server_config.ini')
        if config['general']['workers']:
            configuration['workers'] = int(config['general']['workers'])
        if config['general']['time_limit']:
            configuration['time_limit'] = int(config['general']['time_limit'])
        if config['general']['port']:
            configuration['port'] = int(config['general']['port'])
        if config['general']['numbers_of_files']:
            configuration['numbers_of_files'] = int(config['general']['numbers_of_files'])
    except:
        print('Config file is corrupted. Set default values')
    if not ('time_limit' in configuration):
        configuration['time_limit'] = 100
    if not ('workers' in configuration):
        configuration['workers'] = os.cpu_count()
    if not ('port' in configuration):
        configuration['port'] = 8888
    if not ('numbers_of_files' in configuration):
        configuration['numbers_of_files'] = 4
    return configuration


def Process(task, task_id, process_timeout):
    output_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'tasks', task_id)
    task['files'] = set(os.listdir(output_dir))
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    command = task['command'].strip().split()
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if not task['command']:
        task['state'] = 'Failed'
        task['Error'] = 'Command not found'
        return    
    try:
        command[0] = os.path.join(os.path.dirname(os.path.abspath(__file__)), "tasks", task_id, command[0])
        os.chmod(command[0], 0o744)
    except:
        task['state'] = 'Failed'
        task['Error'] = 'Wrong executing file'
        return

    try:
        stdout_file = open(os.path.join(output_dir, 'stdout.txt'), 'w')
        stderr_file = open(os.path.join(output_dir, 'stderr.txt'), 'w')
        process = subprocess.Popen(command, stdin=subprocess.PIPE,
                               stdout=stdout_file, stderr=stderr_file,
                               cwd = output_dir, universal_newlines=True)
        print("[%s] Task %s running." % (task['client'], task_id))
        try:
            process.wait(timeout=process_timeout)
        except subprocess.TimeoutExpired:
            task['state'] = 'Failed'
            task['Error'] = 'timeout'
            process.kill()
            process.wait()
        task['return_code'] = process.returncode
        if process.returncode == 0:
            task['state'] = 'Done'
        else:
            task['state'] = 'Failed'
            task['Error'] = 'Time Out!'
    except :
            task['Error'] = "PROCESS INITIALIZATION FAILED"
            task['state'] = 'Failed'
            return
    finally:
        stdout_file.close()
        stderr_file.close()
        task['files'] = list(set(os.listdir(output_dir)) - task['files'])
        print("[%s] Task %s is %s.\nEXIT STATUS: %d\n" % (task['client'], task_id, task['state'], process.returncode))



MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 3


def sig_handler(sig, frame):
    tornado.ioloop.IOLoop.instance().add_callback(shutdown)


def shutdown():
    print('\nStopping HTTP server')
    server.stop()

    io_loop = tornado.ioloop.IOLoop.instance()
    deadline = time.time() + MAX_WAIT_SECONDS_BEFORE_SHUTDOWN

    def stop_loop():
        now = time.time()
        if now < deadline and (io_loop._callbacks or io_loop._timeouts):
            io_loop.add_timeout(now + 1, stop_loop)
        else:
            io_loop.stop()
            print('Shutdown')

    for t in threads:
        t.stop()
        t.join()

    stop_loop()


if __name__ == "__main__":
    configuration = get_agent_config()
    print('Configuration:\n', configuration)
    application = Application(configuration)
    threads = []
    server = tornado.httpserver.HTTPServer(application)
    server.listen(configuration['port'])

    print('Server started: http://localhost:'+str(configuration['port']) + '\n')
    for i in range(configuration['workers']):
        t = Worker(application.tasks, application.task_queue, configuration['time_limit'])
        threads.append(t)
        t.start()

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    tornado.ioloop.IOLoop.instance().start()

    print("Exit...")
